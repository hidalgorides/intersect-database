<?php

namespace Tests\Model;

use Intersect\Database\Model\Model;

class TestModel extends Model {
    protected $tableName = 'tests';
}